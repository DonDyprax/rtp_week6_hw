import { useState, useEffect } from "react";
import history from "../helpers/history";

// eslint-disable-next-line no-undef
const {
  REACT_APP_USERNAME,
  REACT_APP_PASSWORD,
  REACT_APP_AUTH_URL,
} = process.env;

export default function useAuth(setLogged: (logged: boolean) => void) {
  const [user, setUser] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const setNewUser = (value: string) => {
    setUser(value);
  };

  const setNewPassword = (value: string) => {
    setPassword(value);
  };

  useEffect(() => {
    if (user === REACT_APP_USERNAME && password === REACT_APP_PASSWORD) {
      fetch(REACT_APP_AUTH_URL!, {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify({
          identifier: REACT_APP_USERNAME,
          password: REACT_APP_PASSWORD,
        }),
      }).then((response) =>
        response
          .json()
          .then((response) => window.localStorage.setItem("jwt", response.jwt))
      );
      setLogged(true);
      history.push("/home");
    } else {
      setLogged(false);
    }
  }, [user, password, setLogged]);

  return [setNewUser, setNewPassword];
}
