interface Genre {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
}

interface CoverArt {
  id: number;
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: Formats;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: ProviderMetadata;
  created_at: string;
  updated_at: string;
}

interface Format {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: string;
  size: number;
  width: number;
  height: number;
  provider_metadata: ProviderMetadata;
}

interface Formats {
  small: Format;
  medium: Format;
  large: Format;
  thumbnail: Format;
}

interface ProviderMetadata {
  pubic_id: string;
  resource_type: string;
}

interface Platform {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  logo: string;
}

interface Publisher {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
}

interface Comment {
  id: number;
  trainee: string;
  game: number;
  body: string;
  created_at: string;
  updated_at: string;
  user: number;
}

export interface GameModel {
  id: number;
  name: string;
  genre: Genre;
  release_year: number;
  price: string;
  created_at: string;
  updated_at: string;
  cover_art: CoverArt;
  platforms: Array<Platform>;
  publishers: Array<Publisher>;
  comments: Array<Comment>;
}
