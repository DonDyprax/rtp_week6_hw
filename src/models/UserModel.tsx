interface Role {
  id: number;
  name: string;
  description: string;
  type: string;
}

export interface UserModel {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  role: Role;
  created_at: string;
  updated_at: string;
  firstName: string;
  lastName: string;
}
