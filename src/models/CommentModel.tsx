import { GameModel } from "./GameModel";

interface User {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  role: number;
  created_at: string;
  updated_at: string;
  firstName: string;
  lastName: string;
}

export interface CommentModel {
  id: number;
  trainee: string;
  game: GameModel;
  body: string;
  created_at: string;
  updated_at: string;
  user: User;
}
