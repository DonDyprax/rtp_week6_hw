import React, { useState, useEffect } from "react";
import "../styles/css/style.css";
import Pagination from "../components/Pagination";
import Games from "../components/Games";
// eslint-disable-next-line no-undef
const { REACT_APP_API_URL } = process.env;

const GamesList = () => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(8);

  useEffect(() => {
    const fetchGames = async () => {
      setLoading(true);
      const res = await fetch(REACT_APP_API_URL + "games", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });

      const data = await res.json();
      setGames(data);
      setLoading(false);
    };

    fetchGames();
  }, []);

  //Get current games
  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = games.slice(indexOfFirstGame, indexOfLastGame);

  //Change page
  const paginate = (pageNumber: number, event: React.MouseEvent) => {
    event.preventDefault();
    setCurrentPage(pageNumber);
  };

  return (
    <div className="list-container">
      <div className="games-list">
        <Games games={currentGames} loading={loading} />
      </div>
      <Pagination
        gamesPerPage={gamesPerPage}
        totalGames={games.length}
        paginate={paginate}
      />
    </div>
  );
};

export default GamesList;
