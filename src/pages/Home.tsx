import React, { useState, useEffect } from "react";

import {
  FaCalendarAlt,
  FaBuilding,
  FaGamepad,
  FaTag,
  FaBook,
} from "react-icons/fa";

import { Link } from "react-router-dom";
import "../styles/css/style.css";
import { GameModel } from "../models/GameModel";
import LatestGames from "../components/LatestGames";
// eslint-disable-next-line no-undef
const { REACT_APP_API_URL } = process.env;

const Home = () => {
  const [latest, setLatest] = useState([]);
  const [loading, setLoading] = useState(false);
  const [newest, setNewest] = useState<GameModel | null>(null);

  const setNewestValue = (value: any) => {
    setNewest(value);
  };

  useEffect(() => {
    const fetchGames = async () => {
      setLoading(true);
      const res = await fetch(REACT_APP_API_URL + "games", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });

      const data = await res.json();
      setLatest(await data.slice(-4));
      setNewestValue(await data[data.length - 1]);
      setLoading(false);
    };

    fetchGames();
  }, []);

  return loading ? (
    <h1>Loading...</h1>
  ) : (
    <div className="home-container">
      <div className="home-content">
        <div className="home-newest">
          <div className="newest-heading">
            <h1>Newest Release </h1>
          </div>
          <div className="newest-content">
            <div className="newest-image">
              {newest?.cover_art ? (
                <Link to={`/games/${newest?.id}`}>
                  <img src={newest.cover_art.url} alt="Game" />
                </Link>
              ) : null}
            </div>
            <div className="newest-details">
              <div className="newest-details-title">
                <FaBook />
                <b>Title: </b>
                {newest?.name}
              </div>
              <div className="newest-details-date">
                <FaCalendarAlt />
                <b>Release Year: </b>
                {newest?.release_year}
              </div>
              <div className="newest-details-publisher">
                <FaBuilding />
                <b>Publisher: </b>
                {newest?.publishers ? newest.publishers[0].name : null}
              </div>
              <div className="newest-details-genres">
                <FaGamepad />
                <b>Genres: </b>
                {newest?.genre ? newest.genre.name : null}
              </div>
              <div className="newest-details-price">
                <FaTag />
                <b>Price: </b>${newest?.price}
              </div>
            </div>
          </div>
        </div>
        <div className="home-latest">
          <h2>Latest Games </h2>
          <LatestGames games={latest} loading={loading} />
        </div>
      </div>
    </div>
  );
};

export default Home;
