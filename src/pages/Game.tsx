import React, { useEffect, useRef, useReducer, useCallback } from "react";
//import { GameModel } from "../models/GameModel";
//import { UserModel } from "../models/UserModel";
//import { CommentModel } from "../models/CommentModel";
import Comments from "../components/Comments";
import "../styles/css/style.css";
import { FaCalendarAlt, FaBuilding, FaGamepad, FaTag } from "react-icons/fa";
// eslint-disable-next-line no-undef
const { REACT_APP_API_URL } = process.env;

const initialState = {
  data: null,
  user: null,
  comments: null,
  loading: true,
};

const actionTypes = {
  FETCH_GAME: "FETCH_GAME",
  FETCH_USER: "FETCH_USER",
  FETCH_COMMENTS: "FETCH_COMMENTS",
};

const reducer = (state: any, action: any) => {
  const { type } = action;
  switch (type) {
    case actionTypes.FETCH_GAME: {
      return { ...state, data: action.payload, loading: false };
    }
    case actionTypes.FETCH_USER: {
      return { ...state, user: action.paylad, loading: false };
    }
    case actionTypes.FETCH_COMMENTS: {
      return { ...state, comments: action.payload, loading: false };
    }
    default: {
      return state;
    }
  }
};

const id = window.location.pathname.split("/")[2];

const Game = () => {
  const submitCommentRef = useRef<HTMLTextAreaElement | null>(null);
  const [state, dispatch] = useReducer(reducer, initialState);
  const { data, user, comments, loading } = state;

  const postNewComment = async () => {
    await fetch(`${REACT_APP_API_URL}games/${id}/comment`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
        Authorization: `Bearer ${window.localStorage.getItem("jwt")}`,
      },
      body: JSON.stringify({ body: `${submitCommentRef?.current?.value}` }),
    });

    if (submitCommentRef && submitCommentRef.current) {
      submitCommentRef.current.value = "";
    }
  };

  const fetchGame = useCallback(async () => {
    const gamesRes = await fetch(`${REACT_APP_API_URL}games/${id}`, {
      method: "GET",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    const gamesData = await gamesRes.json();
    dispatch({ type: actionTypes.FETCH_GAME, payload: gamesData });
  }, []);

  const fetchUser = useCallback(async () => {
    const userRes = await fetch(`${REACT_APP_API_URL}users/me`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("jwt")}`,
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    const userData = await userRes.json();
    dispatch({ type: actionTypes.FETCH_USER, payload: userData });
  }, []);

  const fetchComments = useCallback(async () => {
    const commentsRes = await fetch(
      `${REACT_APP_API_URL}games/${id}/comments`,
      {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }
    );
    const commentsData = await commentsRes.json();
    dispatch({
      type: actionTypes.FETCH_COMMENTS,
      payload: commentsData.reverse(),
    });
  }, []);

  useEffect(() => {
    fetchGame();
    fetchUser();
    fetchComments();
  }, [loading, fetchGame, fetchUser, fetchComments]);

  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <div className="game-container">
      <div className="game-content">
        <div className="game-title">
          <h1>{data?.name}</h1>
        </div>
        <div className="game-image">
          {data?.cover_art ? <img src={data.cover_art.url} alt="Game" /> : null}
        </div>
        <div className="game-details">
          <div className="details-grid">
            <div className="details-date">
              <FaCalendarAlt />
              <b>Release Year: </b>
              {data?.release_year}
            </div>
            <div className="details-publisher">
              <FaBuilding />
              <b>Publisher: </b>
              {data?.publishers ? data.publishers[0].name : null}
            </div>
            <div className="details-genres">
              <FaGamepad />
              <b>Genres: </b>
              {data?.genre ? data.genre.name : null}
            </div>
            <div className="details-price">
              <FaTag />
              <b>Price: </b> ${data?.price}
            </div>
          </div>
        </div>

        <div className="game-comments">
          <h2>Comments</h2>
          <div className="comments">
            <div className="new-comment">
              <div className="new-comment-user">
                {user ? (
                  <p>
                    Current User: <b>{user.username}</b>
                  </p>
                ) : null}
              </div>
              <textarea
                ref={submitCommentRef}
                name="new-comment"
                id="new-comment"
                cols={30}
                rows={5}
              ></textarea>
              <input
                type="submit"
                id="submitComment"
                value="Submit"
                onClick={() => {
                  if (submitCommentRef?.current?.value !== "") {
                    postNewComment();
                  }
                }}
              />
            </div>
            <Comments comments={comments} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Game;
