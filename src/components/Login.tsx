import React, { useRef } from "react";
import useAuth from "../hooks/useAuth";

import "../styles/css/style.css";

interface Props {
  setLogged: (logged: boolean) => void;
}

const Login: React.FC<Props> = ({ setLogged }) => {
  const userRef = useRef<HTMLInputElement | null>(null);
  const passwordRef = useRef<HTMLInputElement | null>(null);
  const [setUser, setPassword] = useAuth(setLogged);

  const submitCredentials = (ev: React.FormEvent) => {
    ev.preventDefault();
    if (userRef && userRef.current && passwordRef && passwordRef.current) {
      setUser(userRef.current.value);
      setPassword(passwordRef.current.value);
    }
  };

  return (
    <div className="login-container">
      <div className="login">
        <form method="post" onSubmit={(ev) => submitCredentials(ev)}>
          <label htmlFor="username">Username</label>
          <input ref={userRef} type="text" name="username" id="username" />
          <br />
          <label htmlFor="password">Password</label>
          <input
            ref={passwordRef}
            type="password"
            name="password"
            id="password"
          />
          <br />
          <input id="login-submit" type="submit" value="Login" />
        </form>
      </div>
    </div>
  );
};

export default Login;
