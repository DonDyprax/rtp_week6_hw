import React from "react";
import "../styles/css/style.css";

interface Props {
  gamesPerPage: number;
  totalGames: number;
  paginate: (pageNumber: number, event: React.MouseEvent) => void;
}

const Pagination: React.FC<Props> = ({
  gamesPerPage,
  totalGames,
  paginate,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalGames / gamesPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="pagination-container">
      <ul className="pagination">
        {pageNumbers.map((number) => (
          <li key={number} className="page-item">
            <b>
              <a
                onClick={(event) => paginate(number, event)}
                href="/"
                className="page-link"
              >
                {number}
              </a>
            </b>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Pagination;
