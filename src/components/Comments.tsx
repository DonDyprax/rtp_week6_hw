import React from "react";
import { CommentModel } from "../models/CommentModel";
import Comment from "./Comment";

interface Props {
  comments: Array<CommentModel>;
}

const Comments: React.FC<Props> = ({ comments }) => {
  if (!comments) {
    return <h3>This post has no comments</h3>;
  } else {
    return (
      <>
        {comments.map((comment) => (
          <Comment key={comment.id} comment={comment} />
        ))}
      </>
    );
  }
};

export default Comments;
