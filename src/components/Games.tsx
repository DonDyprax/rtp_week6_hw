import React from "react";

import GameCard from "./GameCard";

interface Props {
  games: Array<any>;
  loading: boolean;
}

const Games: React.FC<Props> = ({ games, loading }) => {
  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <>
      {games.map((game) => (
        <GameCard key={game.id} data={game} />
      ))}
    </>
  );
};

export default Games;
