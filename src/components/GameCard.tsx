import React from "react";
import { GameModel } from "../models/GameModel";

import "../styles/css/style.css";
import { FaCalendarAlt } from "react-icons/fa";

import { Link } from "react-router-dom";

interface Props {
  data: GameModel;
}

const GameCard: React.FC<Props> = ({ data }) => {
  return (
    <Link to={`/games/${data.id}`}>
      <div className="gamecard">
        <div className="gamecard-image">
          {data.cover_art ? (
            <img src={data.cover_art.formats.small.url} alt="Game" />
          ) : null}
        </div>
        <div className="gamecard-details">
          <div className="card-details-date">
            {" "}
            <FaCalendarAlt /> {data.release_year}
          </div>
          <div className="card-details-title">{data.name}</div>
          <div className="card-details-price">${data.price}</div>
        </div>
      </div>
    </Link>
  );
};

export default GameCard;
