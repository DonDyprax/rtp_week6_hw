import React from "react";
import { CommentModel } from "../models/CommentModel";

interface Props {
  comment: CommentModel;
}

const Comment: React.FC<Props> = ({ comment }) => {
  if (!comment) {
    return <h3>Loading...</h3>;
  } else {
    return (
      <div className="comment">
        {comment.user ? <h3>{comment.user.username}</h3> : null}
        <p>{comment.body}</p>
      </div>
    );
  }
};

export default Comment;
