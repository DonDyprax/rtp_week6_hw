import React from "react";

import "../../styles/css/style.css";
import logo from "../../assets/week4logo.png";
import { FaHome, FaGamepad, FaQuestionCircle } from "react-icons/fa";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <div className="logo">
        <img src={logo} alt="Site Logo" />
      </div>
      <div className="links">
        <ul>
          <Link to="/home">
            <li>
              {FaHome}
              Home
            </li>
          </Link>

          <Link to="/games">
            <li>
              {FaGamepad}
              Games
            </li>
          </Link>

          <Link to="/about">
            <li>
              {FaQuestionCircle}
              About
            </li>
          </Link>
        </ul>
      </div>
    </div>
  );
};

export default React.memo(Navbar);
