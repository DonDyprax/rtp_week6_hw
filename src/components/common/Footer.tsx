import React from "react";

import "../../styles/css/style.css";
import {
  FaFacebook,
  FaTwitter,
  FaInstagram,
  FaYoutube,
  FaSnapchat,
} from "react-icons/fa";

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-content">
        <h3>NXT LVL</h3>
        <p>
          NXT LVL is an online videogame store. We strive to provide our
          customers with the best games at the best prices.
        </p>
        <ul className="socials">
          <li>
            <a href="https://www.facebook.com">
              <FaFacebook />
            </a>
          </li>
          <li>
            <a href="https://www.twitter.com">
              <FaTwitter />
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com">
              <FaInstagram />
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com">
              <FaYoutube />
            </a>
          </li>
          <li>
            <a href="https://www.snapchat.com">
              <FaSnapchat />
            </a>
          </li>
        </ul>
      </div>

      <div className="footer-bottom">
        <p>
          Copyright &copy; 2021 NXT LVL. Designed by <span>DonDyprax</span>
        </p>
      </div>
    </div>
  );
};

export default React.memo(Footer);
