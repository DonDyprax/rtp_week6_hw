import React, { useState, Suspense, lazy } from "react";
import "../styles/css/style.css";
import { Router, Switch, Route } from "react-router-dom";
import ProtectedRoute from "../pages/ProtectedRoute";
import history from "../helpers/history";

const Navbar = lazy(() => import("./common/Navbar"));
const Footer = lazy(() => import("./common/Footer"));
const GamesList = lazy(() => import("../pages/GamesList"));
const Game = lazy(() => import("../pages/Game"));
const Home = lazy(() => import("../pages/Home"));
const Login = lazy(() => import("./Login"));

enum Routes {
  HOME = "/home",
  ABOUT = "/about",
  GAMES = "/games",
  GAME = "/games/:id",
  LOGIN = "/login",
}

const App = () => {
  const [logged, setLogged] = useState(false);

  return logged ? (
    <Router history={history}>
      <Suspense fallback={<h2>Loading...</h2>}>
        <div className="App">
          <Navbar />
          <Switch>
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.HOME}
              exact
              component={Home}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.GAMES}
              exact
              component={GamesList}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.ABOUT}
              exact
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.GAME}
              exact
              component={Game}
            />
          </Switch>
          <Footer />
        </div>
      </Suspense>
    </Router>
  ) : (
    <Router history={history}>
      <Suspense fallback={<h2>Loading...</h2>}>
        <div className="App">
          <Switch>
            <Route
              path={Routes.LOGIN}
              exact
              render={() => <Login setLogged={setLogged} />}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.HOME}
              component={Home}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.HOME}
              exact
              component={Home}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.GAMES}
              exact
              component={GamesList}
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.ABOUT}
              exact
            />
            <ProtectedRoute
              isAuthenticated={logged}
              authenticationPath={Routes.LOGIN}
              path={Routes.GAME}
              exact
              component={Game}
            />
          </Switch>
        </div>
      </Suspense>
    </Router>
  );
};

export default App;
