import React from "react";
import "../styles/css/style.css";
import Games from "./Games";
import { GameModel } from "../models/GameModel";

interface Props {
  games: Array<GameModel>;
  loading: boolean;
}

const LatestGames: React.FC<Props> = ({ games, loading }) => {
  return (
    <div className="latest-games">
      <Games games={games} loading={loading} />
    </div>
  );
};

export default LatestGames;
